#include "8-puzzle.h"


//values constructor (initializes g to zero)

state::state(int** bd, state* prnt) {
    //ID = nextID++;
    board = init(bd); //each state's dynamic memory is created here
    parent = prnt;
    g = 0;
    state* pp;
//    if (prnt != 0)
//        pp = (*prnt).getPointer();
//    else
//        pp = 0;
    //cout << "parent's parent:" << pp << " this's parent:" << parent << endl;
    //cout << "ID:" << ID << endl << endl;
}

//copy

state::state(const state& other) {
    //ID = nextID++;
    board = init(other.board); //creates a new dynamic array here
    parent = other.parent;
    g = other.g;
    //cout << "ID:" << ID << endl << endl;
}

//default

state::state() {
    //ID = nextID++;
    state* p;
    parent = p;
    board = init();
    g = 0;
    //cout << "ID:" << ID << endl << endl;
}

//destructor (deletes dynamic array)

state::~state() {
    //cout << "deleting state with ID:" << ID << endl << endl;

    //delete dynamic array
    del(board);
}

//used to check for self-assignment true if
//board, parent and g variables are the same

bool state::operator==(const state& other)const {
    return (*this).equals(other) && g == other.g && parent == other.parent;
}

//used to check if boards are the same, 
//disregards parent and g variables

bool state::equals(const state& other)const {
    bool equal = true;
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            equal = equal && (board[i][j] == other.board[i][j]);
    return equal;
}

//true if the two states are not equal
//uses the == operator

bool state::operator!=(const state& other)const {
    return!(*this == other);
}

//true if the f value of the left hand side
//is less than the f value of the right hand side

bool operator<(const state& lhs, const state& rhs) {
    return f(lhs) < f(rhs);
}

//true if the f value of the left hand side
//is greater than the f value of the right hand side

bool state::operator>(const state& other)const {
    return f(*this) > f(other);
}

//assigns the values of the other to this and
//returns a reference to this.

state& state::operator=(const state& other) {

    //check for self-assignment
    if (*this != other) {

        //deallocate old memory
        del(board);

        //allocate new memory and copy variables
        board = init(other.board);
        parent = other.parent;
        g = other.g;
    }
    return *this;
}

//prints the 

void state::print() {
    {
        print2D(board);
    }
    //cout << "f: " << f(*this) << endl;
    //cout << "g: " << g << endl;
    //cout << "ID: " << ID << endl;
    //cout << "Parent address: " << parent << endl << endl;

}

/////////////////////////////
//END STATE CLASS FUNCTIONS
/////////////////////////////






