/* 
 * File:   8-puzzle.h
 * Author: carlchapman
 *
 * Created on October 22, 2011, 6:54 PM
 */

#ifndef _8_PUZZLE_H
#define	_8_PUZZLE_H

#include <iostream>
#include <sstream>
#include <list>

using namespace std;

//static int counter = 0;
//static int nextID = 0;

//this array holds the goal state.  If changed, the 
//solvability testing algorithm will be skipped in
//the main method because it doesn't apply
static int goalState[] = {1, 2, 3, 8, 0, 4, 7, 6, 5};

//this array holds arrays that hold
//the number of moves from a given square other squares are
//and zeros for the empty tile
static int mdist[10][9] = {
    {0, 1, 2, 1, 2, 3, 2, 3, 4}, //NorthWest
    {1, 0, 1, 2, 1, 2, 3, 2, 3}, //North 
    {2, 1, 0, 3, 2, 1, 4, 3, 2}, //NorthEast
    {1, 2, 3, 0, 1, 2, 1, 2, 3}, //West
    {2, 1, 2, 1, 0, 1, 2, 1, 2}, //Center
    {3, 2, 1, 2, 1, 0, 3, 2, 1}, //East
    {2, 3, 4, 1, 2, 3, 0, 1, 2}, //SouthWest
    {3, 2, 3, 2, 1, 2, 1, 0, 1}, //South
    {4, 3, 2, 3, 2, 1, 2, 1, 0}, //SouthEast
    {0, 0, 0, 0, 0, 0, 0, 0, 0} //zero
};

//a static variable tracking the current heuristic
//used by states to determine their f value
static int heuristic = 0;

class state {
public:

    //constructors & destructor
    state(int** bd, state* prnt = 0);
    state(const state& other);
    state();
    ~state();


    //operators, print and addStates
    bool operator==(const state& other)const;
    bool equals(const state& other)const;
    bool operator!=(const state& other)const;
    friend bool operator<(const state& lhs, const state& rhs);
    bool operator>(const state& other)const;
    state& operator=(const state& other);
    void print();



    //getters and setters

//    int getID() const {
//        return ID;
//    }

    int getG() const {
        return g;
    }

    int** getBoard() const {
        return board;
    }

    state& getParent() const {
        return *parent;
    }

//    state* getPointer() const {
//        return parent;
//    }

    void setG(int n) {
        g = n;
    }

    void setParent(state& other) {
        parent = &other;
    }


private:
    //int ID;
    int g;
    int** board;
    state* parent;
};

//functions used to solve
void addStates(list<state>& open, list<state>& closed, state& st);
void changeLists(list<state>& open, list<state>& closed, state& st, list<state>& children);
bool equalsGoal(const state& st);
state generate(int newG, state* st, int**& p, int fromR, int fromC, int toR, int toC);
string op(const state& before, const state& after);
void printSolution(state& st);
void solve(const state& initialState);
void swap(int**& p, int fromR, int fromC, int toR, int toC);


//heuristics and f
int h1(const state& st);
int h2(const state& st);
int h3(const state& st);
int f(const state& st);

//other utilities
int** init(int** p = 0);
void del(int** p);
void findZero(int** p, int& row, int& col);
void printBoard(int* a);
void printList(list<state>& L);
void print2D(int** a);
int indexOf(int*, int n);


#endif	/* _8_PUZZLE_H */

