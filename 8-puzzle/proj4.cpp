/* 
 * File:   proj4.cpp
 * Author: carlchapman
 *
 * Created on October 22, 2011, 6:35 PM
 */
#include "8-puzzle.h"

int main() {

    //initialize all of input[] to -1
beginning:
    string s = "";
    int input[9];
    fill_n(input, 9, -1);
    int x = 0;
    bool isStandard = true;
    int standardGoalState[] = {1, 2, 3, 8, 0, 4, 7, 6, 5};
    list<int> checklist(goalState, goalState + 9);
    int inversions = 0;


    //print goal state and get input
    cout << "Goal State:\n\n";
    printBoard(goalState);
    cout << "\nInitial State (input):";
    getline(cin, s);
    cout << endl;


    //check for the end signal
    if (s[0] == 'E') goto end;

        //try to transfer input from the string to an array of ints
    else {
        stringstream ss(s);
        while (ss.good() && x < 9) {
            ss >> input[x];

            //remove each int from the checklist as it is put into the array
            checklist.remove(input[x++]);
        }

        //if the checklist is not empty, then at least one of the required digits was not entered
        if (!checklist.empty()) {
            cout << "Please enter exactly 9 unique digits between 0 and 8\nseparated by spaces, or 'E' to exit.\n\n";
            goto beginning;
        }
    }

    //echo correct input back to user
    printBoard(input);

    //test for solvability
    //cout<<"testing\n";

    for (int i = 0; i < 9; i++) {
        if (goalState[i] != standardGoalState[i]) {
            isStandard = false;
            break;
        }
    }
    if (isStandard) {
        for (int i = 7; i >= 0; i--) {
            for (int j = i; j < 9; j++)
                if (input[j] != 0 && input[i] > input[j])
                    inversions++;
        }
    } else
        inversions = 1;

    if ((inversions % 2 == 0))
        cout << "No solution exists!\n\n";
    else {
        //int input[] = {4, 1, 2, 3, 5, 0, 8, 6, 7};

        //create new dynamic 2D array
        int** p = new int*[3];
        for (int i = 0; i < 3; i++) {
            p[i] = new int[3];
        }

        //initialize the new 2D array
        x = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                p[i][j] = input[x++];
            }
        }

        //cout<<"create initial state\n";
        state initialState(p);
        heuristic = 1;
        solve(initialState);
        //counter = 0;
        heuristic = 2;
        cout << "using manhattan distance:" << endl << endl;
        printBoard(input);
        solve(initialState);
        //counter = 0;
        heuristic = 3;
        cout << "using double moves:" << endl << endl;
        printBoard(input);
        solve(initialState);

        del(p);
    }

    goto beginning;
end:
    return 0;
}

//adds new states to the open and closed lists
//using the given state st to generate children

void addStates(list<state>& open, list<state>& closed) {
    //get a reference to the item just added to closed
    state& st = closed.back();
    //cout<<"heuristicType:"<<heuristic<<endl;
    //cout << "addstates called" << endl;
    //find the row and column of zero for st
    int** copy;
    //cout << "copying state's board:" << endl;
    copy = init(st.getBoard());
    int rowZ = 0;
    int colZ = 0;
    findZero(copy, rowZ, colZ);
    //cout << "rowZ:" << rowZ << " colZ:" << colZ << endl << endl;

    //generate children of st 'T'and put onto child list
    int newG = st.getG() + 1;
    list<state> children;



    //if the center is empty
    if (rowZ == 1 && colZ == 1) {
        children.push_front(generate(newG, &st, copy, 0, 1, 1, 1));
        children.push_front(generate(newG, &st, copy, 1, 2, 1, 1));
        children.push_front(generate(newG, &st, copy, 2, 1, 1, 1));
        children.push_front(generate(newG, &st, copy, 1, 0, 1, 1));

        //if a corner is empty
    } else if (rowZ % 2 == 0 && colZ % 2 == 0) {
        children.push_front(generate(newG, &st, copy, rowZ, colZ, rowZ, 1));
        children.push_front(generate(newG, &st, copy, rowZ, colZ, 1, colZ));
        if (heuristic == 3) {
            //cout << "trying heuristic 3...old state:" << endl;
            // st.print();
            swap(copy, rowZ, colZ, 1, colZ);
            children.push_front(generate(newG, &st, copy, 1, colZ, ((rowZ + 2) % 4), colZ));
            //cout << "new state:" << endl;
            //(children.front()).print();
            swap(copy, rowZ, colZ, 1, colZ);
            swap(copy, rowZ, colZ, rowZ, 1);
            children.push_front(generate(newG, &st, copy, rowZ, 1, rowZ, ((colZ + 2) % 4)));
            swap(copy, rowZ, colZ, rowZ, 1);
        }

        //if the top or bottom middle is empty
    } else if (rowZ % 2 == 0) {
        children.push_front(generate(newG, &st, copy, rowZ, colZ, 1, 1));
        children.push_front(generate(newG, &st, copy, rowZ, colZ, rowZ, 0));
        children.push_front(generate(newG, &st, copy, rowZ, colZ, rowZ, 2));
        children.push_front(generate(newG, &st, copy, rowZ, colZ, 1, colZ));
        if (heuristic == 3) {
            //cout << "trying heuristic 3...old state:" << endl;
            //st.print();
            swap(copy, rowZ, colZ, 1, colZ);
            children.push_front(generate(newG, &st, copy, 1, colZ, ((rowZ + 2) % 4), colZ));
            //cout << "new state:" << endl;
            //(children.front()).print();
            swap(copy, rowZ, colZ, 1, colZ);
        }

        //if one of the sides are empty
    } else {
        children.push_front(generate(newG, &st, copy, rowZ, colZ, 1, 1));
        children.push_front(generate(newG, &st, copy, rowZ, colZ, 0, colZ));
        children.push_front(generate(newG, &st, copy, rowZ, colZ, 2, colZ));
        children.push_front(generate(newG, &st, copy, rowZ, colZ, 1, colZ));
        if (heuristic == 3) {
            swap(copy, rowZ, colZ, rowZ, 1);
            children.push_front(generate(newG, &st, copy, rowZ, 1, rowZ, ((colZ + 2) % 4)));
            //cout << "trying heuristic 3...old state:" << endl;
            //st.print();
            //cout << "new state:" << endl;
            //(children.front()).print();
            swap(copy, rowZ, colZ, rowZ, 1);
        }
    }

    //cout << "children generated:" << endl;
    //printList(children);


    //deallocate memory used to generate new states
    //cout << "deleting copy" << endl << endl;
    del(copy);
    changeLists(open, closed, st, children);
}


//for every unique child or child with a lower f value
//than a state on the list, the list is updated to
//either include new children or their better path and g value

void changeLists(list<state>& open, list<state>& closed, state& st, list<state>& children) {

    //cout << "changeLists called" << endl << endl;
    //for every child of the given state
    while (!children.empty()) {
        bool childFound = false;

        //check if current child is on open
        state& st = children.front();
        list<state>::iterator it = open.begin();
        list<state>::iterator end = open.end();
        while (it != end) {

            //if child is on open, evaluate and don't add it
            if (st.equals(*it)) {
                //cout << "child equals state on open" << endl;
                childFound = true;

                //if child has smaller f than state on open, adjust
                if (f(st) < f(*it)) {
                    //cout << "child found has smaller f:" << endl;
                    //cout << "before adjustments:" << endl;
                    //(*it).print();
                    //reset its parent pointer and G value
                    (*it).setParent(st.getParent());
                    (*it).setG(st.getG());
                    //cout << "after adjustments:" << endl;
                    //(*it).print();
                }
            }
            it++;
        }
        //check if current child is on closed
        list<state>::iterator it2 = closed.begin();
        list<state>::iterator end2 = closed.end();
        while (it2 != end2) {

            //if child is on closed, evaluate and don't add it
            if (st.equals(*it2)) {
                childFound = true;

                //cout << "child equals state on closed" << endl;
                //if the child is found on closed,
                //remove it from closed and put onto open
                if (f(st) < f(*it2)) {
                    //cout << "child found with smaller f:" << endl;
                    //(*it).print();

                    //then erase the old value
                    //cout << "erasing old value" << endl;
                    closed.erase(it2);
                    //and re-open the state for 
                    //examination with this parent
                    //cout << "pushing state onto open" << endl;
                    open.push_front(st);

                }
            }
            it2++;
        }

        //if children were not found in open or closed
        //then put onto open
        if (!childFound) {
            //cout << "no child found - pushing onto open." << endl;
            open.push_front(st);
        }

        //remove the processed child from the list
        //cout << "popping front of children" << endl;
        children.pop_front();
    }
}


//true if the state's board matches
//the goal state

bool equalsGoal(const state& st) {
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            if (st.getBoard()[i][j] != goalState[i * 3 + j])
                return false;
    return true;
}


//generates a state by swapping board elements at the
//from and to rows and columns in a given 2D int array,
//and using that, the new g value and the parent to create
//a new state object (swaps the board elements back after use)

state generate(int newG, state* st, int**& p, int fromR, int fromC, int toR, int toC) {
    swap(p, fromR, fromC, toR, toC);
    //cout << "create child:" << endl;
    state toReturn(p, st);
    toReturn.setG(newG);
    swap(p, fromR, fromC, toR, toC);
    return toReturn;
}

//returns a string representing the operation
//that transformed the state

string op(const state& before, const state& after) {
    int rowZBefore = 0;
    int colZBefore = 0;
    int rowZAfter = 0;
    int colZAfter = 0;
    findZero(before.getBoard(), rowZBefore, colZBefore);
    findZero(after.getBoard(), rowZAfter, colZAfter);

    //if the move was horizontal
    if (rowZBefore == rowZAfter) {
        if ((colZBefore == 0 && colZAfter == 1) || (colZBefore == 1 && colZAfter == 2))
            return "LEFT";
        else if ((colZBefore == 1 && colZAfter == 0) || (colZBefore == 2 && colZAfter == 1))
            return "RIGHT";
        else if (colZBefore == 2 && colZAfter == 0)
            return "DBL-RIGHT";
        else if (colZBefore == 0 && colZAfter == 2)
            return "DBL-LEFT";
        else
            return "ILLEGAL OPERATION";

        //if the move was vertical
    } else if (colZBefore == colZAfter) {
        if ((rowZBefore == 0 && rowZAfter == 1) | (rowZBefore == 1 && rowZAfter == 2))
            return " UP";
        else if ((rowZBefore == 2 && rowZAfter == 1) || (rowZBefore == 1 && rowZAfter == 0))
            return "DOWN";
        else if (rowZBefore == 2 && rowZAfter == 0)
            return "DBL-DOWN";
        else if (rowZBefore == 0 && rowZAfter == 2)
            return "DBL-UP";
        else
            return "ILLEGAL OPERATION";

    }

    //if the move was illegal
    return "ILLEGAL OPERATION";
}

//prints the solution by following the path
//of the final state up it's parent 
//references until it reaches the initial state
//and then printing the operation and next states
//till finished

void printSolution(list<state>& closed) {
    int moveCount = 0;
    state& endState = closed.back();

    state* p = &endState;
    list<state> solution;
    while ((*p).getG() != 0) {
        solution.push_front(*p);
        p = &(*p).getParent();
    }
    //printList(solution);
    //cout << "!!!!!PRINTING SOLUTION!!!!!!!" << endl;
    //printList(closed);

    list<state>::iterator it = solution.begin();
    list<state>::iterator end = solution.end();

    while (it != end) {
        cout << endl << op((*it).getParent(), *it) << endl << endl;
        (*it).print();
        moveCount++;
        it++;
    }
    cout << endl << moveCount << " moves in total." << endl << endl;
}



//solves a puzzle using the A* algorithm

void solve(const state& initialState) {

    list<state> open;
    list<state> closed;
    //cout << "solve called: pushing onto list" << endl;
    open.push_front(initialState);

    while (!open.empty()) {
        //cout << "cycle number:" << counter++ << endl;
        //puts the lowest f values in the front
        open.sort();
        //cout << "open after sorting:" << endl;
        //printList(open);

        //check all lowest f value states
        //if one is the goal, print solution
        list<state>::iterator it = open.begin();
        list<state>::iterator end = open.end();
        //cout << "copy front element:" << endl;
        state st = open.front();
        //cout << endl;
        int min = f(st);
        while (it != end && f(*it) == min) {
            //cout<<"equals goal?"<<equalsGoal(*it)<<endl;
            //(*it).print();
            if (equalsGoal(*it)) {
                closed.push_back(*it);
                printSolution(closed);
                return;
            }
            it++;
        }


        ///put the state with the lowest 'f' from open
        //onto closed and remove from open
        //cout << "pushing onto closed:" << endl;
        closed.push_back(st);
        //cout << "contents of closed:" << endl;
        //printList(closed);
        //cout << "popping front of open:" << endl;
        open.pop_front();

        //compare the children of state st with states
        //on the open list, adjusting the g and parent
        //values to keep the minimum path, or remove from
        //the closed list if the new child has a minimal
        //path (adding to open), and putting it onto open
        //if there it is on neither list.
        addStates(open, closed);
    }
    cout << "No solution was found." << endl;
}

//swaps two board elements at the given rows and columns

void swap(int**& p, int fromR, int fromC, int toR, int toC) {
    int temp = p[fromR][fromC];
    p[fromR][fromC] = p[toR][toC];
    p[toR][toC] = temp;
}



/////////////////////////
//HEURISTICS AND F
/////////////////////////

//returns the number of mismatched tiles

int h1(const state& st) {
    int x = 0, mismatched = 0;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            int tile = st.getBoard()[i][j];
            //cout<<"tile:"<<tile<<" x:"<<x<<" goalstate[x]:"<<goalState[x]<<" tile!=0?"<<(tile!=0)<<" tile!=goalstate[x]?"<<(tile!=goalState[x])<<endl;
            if (tile != goalState[x++] && tile != 0)
                mismatched++;
        }
    }
    //cout<<"mismatched:"<<mismatched<<endl<<endl;
    return mismatched;
}

//returns the sum of the 'manhattan distances' 
//between the tiles and the goal state tiles

int h2(const state& st) {

    int sum = 0;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {

            //this finds the index of the current i,j coordinate in the 
            //goal state array and then fetches the correct manhattan distance
            //from a static array of answers (this allows the goalstate to be
            //altered without invalidating this method)
            int index = 9;
            if (st.getBoard()[i][j] != 0)
                index = indexOf(goalState, st.getBoard()[i][j]);
            sum = sum + mdist[index][i * 3 + j];
        }
    }
    return sum;
}

//accounts for both single and double moves
//each double move reduces the 'manhattan distance'
//by at most 2, and the last move must be a single move

int h3(const state& st) {
    int H2 = h2(st);
    if (H2 % 2 == 0)
        return (H2 + 1) / 2;
    else
        return (H2 + 2) / 2;
}

//returns the correct f value
//depending on the current

int f(const state& st) {
    if (heuristic == 1) {
        return (st.getG() + h1(st));
    } else if (heuristic == 2) {
        return (st.getG() + h2(st));
    } else if (heuristic == 3) {
        return (st.getG() + h3(st));
    } else
        return -1;
}



////////////////////////////
//UTILITIES
////////////////////////////

//initializes a dynamic 2D int array 'b' of size 3,3
//if the argument p is not default, it also copies p into b
//then returns b (this assumes p is at least 3x3)

int** init(int** p) {
    //cout << "init:" << endl;
    int** b = new int*[3];
    for (int i = 0; i < 3; i++) {
        b[i] = new int[3];
        for (int j = 0; j < 3; j++) {
            if (p != 0) {
                b[i][j] = p[i][j];
            } else
                b[i][i] = 0;
        }
    }
    //print2D(b);
    return b;
}



//frees memory of a 2D 3x3 array 

void del(int** p) {
    for (int i = 0; i < 3; i++) {
        delete[] p[i];
    }
    delete[] p;
}

//finds the first row and column containing zero 
//and changes the value of two reference ints

void findZero(int** p, int& row, int& col) {

    //find the row and column of the empty square
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (p[i][j] == 0) {

                //assign the row and col
                //to the referenced ints
                row = i;
                col = j;
                return;
            }
        }
    }
}

//prints a board from a 1D array containing 9 ints

void printBoard(int*a) {
    //assumes that the array has 9 digits representing a board
    //for the 8-puzzle
    int x = 0;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            x = i * 3 + j;
            if (*(a + x) == 0)
                cout << "  ";
            else
                cout << *(a + x) << " ";
        }
        cout << endl;
    }
}

void printList(list<state>& L) {
    int listCounter = 0;
    list<state>::iterator it = L.begin();
    list<state>::iterator end = L.end();
    while (it != end) {
        cout << "list element#" << listCounter++ << ": " << endl;
        (*it).print();
        it++;
    }
    if (listCounter > 5)
        exit(5);
}

void print2D(int** a) {
    //assumes a 3x3 int array
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (a[i][j] == 0)
                cout << "  ";
            else
                cout << a[i][j] << " ";
        }
        cout << endl;
    }
}

int indexOf(int* array, int n) {
    for (int i = 0; i < 9; i++) {
        if (array[i] == n) {
            //cout<<"index found:"<<i<<endl;
            return i;
        }
    }
    cerr << "number in board not found in goal state" << endl;
    return -1;
}

//junk from trying to use priority queue:
//    priority_queue<state,vector<state>,comp> OPEN;
//    priority_queue<state,vector<state>,comp> CLOSED;
//    OPEN.push(initialState);
//class comp {
//public:
//
//    bool operator()(state& lhs, state& rhs) const{
//        return f(lhs)<f(rhs);
//    }
//};

